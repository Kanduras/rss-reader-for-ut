import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3

Page {
    visible: false

    property var db

    header: PageHeader {
        title: i18n.tr("Settings")
    }

    style: Rectangle {
        anchors.fill: parent
        color: theme.palette.normal.background
    }

    Column {
        id: cSettings
        anchors.margins: units.gu(1)
        spacing: units.gu(1)

        anchors{
            top: header.bottom
            left: parent.left
            right: parent.right
        }

        Label {
            id: lAddFeed

            textSize: Label.Large
            text: i18n.tr("Add a new feed:")
        }

        Label {
            id: lName
            textSize: Label.Medium
            text: i18n.tr("Name: ")
        }

        TextField {
            id: tfName
            anchors.left: parent.left

            width: cSettings.width
            placeholderText: i18n.tr('e.g.: "Linuxnews"')

            onTextChanged: text.length > 0 && tfFeed.text.length > 0 ? bAddFeed.enabled = true : bAddFeed.enabled = false

        }

        Label {
            id: lUrl
            textSize: Label.Medium
            text: i18n.tr("URL: ")
        }

        TextField {
            id: tfFeed

            anchors.left: tfName.left
            anchors.right: tfName.right

            placeholderText: i18n.tr('e.g.: "https://linuxnews.de/feed"')

            onTextChanged: text.length > 0 && tfName.text.length > 0 ? bAddFeed.enabled = true : bAddFeed.enabled = false
        }

        Button {
            id: bAddFeed

            anchors.left: tfFeed.left
            anchors.right: tfFeed.right

            enabled: false
            color: LomiriColors.green

            text: i18n.tr("Add Feed")

            onClicked: {
                db.newFeed(tfName.text, tfFeed.text)
                pStack.pop()
            }
        }
    }
}
