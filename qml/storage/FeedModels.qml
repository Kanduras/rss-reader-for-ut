import QtQuick 2.7
import Lomiri.Components 1.3

Item {

    Database{
        id: db
    }

    property var feedsModel: ListModel{}

    Component.onCompleted: {
        var i
        // init feeds
        var feeds = db.getFeeds()
        for (i=0;i<feeds.length;i++){
            var feed = feeds[i]

            var feed_insert = {
                feedId:   feed.id,
                name:    feed.name,
                url: feed.url,
            }

            feedsModel.append(feed_insert)
        }      
    }

    function newFeed(name, url){
        // add to database
        var id = db.insertFeed(name, url)
        if (id===-1)
            return false

        var feed = {
            feedId: id,
            name: name,
            url: url
        }
        // add to models
        feedsModel.append(feed)

        return feed
    }
    
    function removeFeed(id){
        // remove from database
        if (db.deleteFeed(id)){
            // remove from models
            for (var i=0;i<feedsModel.count;i++){
                if (feedsModel.get(i).feedId===id){
                    feedsModel.remove(i)
                    break
                }
            }
        }
        return false
    }
}