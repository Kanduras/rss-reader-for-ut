import QtQuick 2.7
import QtQuick.LocalStorage 2.0

Item {
    property var db: null

    function openDB() {
        if(db !== null) return;

        db = LocalStorage.openDatabaseSync("rssreader-feeds", "", "RSS Reader, storage for feeds", 100000);
        if (db.version === "") {
            db.changeVersion("", "0.1",
                function(tx) {
                    tx.executeSql('CREATE TABLE IF NOT EXISTS Feeds(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, url TEXT)');

                    console.log('Database created, v0.1');
                });
            db = LocalStorage.openDatabaseSync("rssreader-feeds", "", "RSS Reader, storage for feeds", 100000);
        }
    }

    function insertFeed(feedName, feedUrl){
        if(!db) openDB();
        var res;

        try {
            db.transaction(function(tx){
                res = tx.executeSql('INSERT INTO Feeds(name, url) VALUES (?,?)', [feedName, feedUrl]);
            });

            return parseInt(res.insertId);
        } catch (e) {
            console.err("Cannot insert feed: " + e);
            return -1;
        }
        
    }

    function deleteFeed(feedId){
        if(!db) openDB();
        try {
            db.transaction(function(tx){
                tx.executeSql('DELETE FROM Feeds WHERE id = ?', [feedId])
            });
            return true;
        } catch (e) {
            console.err("Cannot delete Feed with id: " + feedId + " " + e);
            return false;   
        }
    }

    function getFeeds(){
        openDB();
        var ret;
        db.readTransaction(
            function(tx){
                ret = tx.executeSql('SELECT * FROM Feeds');
            }
        )
        return ret.rows;
    }
}