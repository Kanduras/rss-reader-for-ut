import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import QtWebEngine 1.1
import QtWebView 1.1

Page {
    visible: false

    property var description
    property var headline
    property var link

    header: PageHeader {
        title: headline
    }

    style: Rectangle {
        anchors.fill: parent
        color: theme.palette.normal.background
    }

    Flickable {
        id: list
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        width: parent.width

        //contentHeight: web.paintedHeight

        WebView {
            id: web
            anchors.fill: parent

            onUrlChanged: {
                if(url == link){
                    pStack.pop()
                    Qt.openUrlExternally( url )
                }
            }

            Component.onCompleted: {
                var cssDarkMode = ''

                if(Theme.name == 'Lomiri.Components.Themes.SuruDark'){
                    cssDarkMode = ':root {color-scheme: dark;}'
                }

                loadHtml('
                    <style>'
                    +
                    cssDarkMode
                    +
                    'img {
                        margin-bottom: 5px;

                        width:100%;
                        height: auto;
                    }
                    </style>
                    <div  style="font-size: 3em; margin:2%;">' + description + '</div>', "null")
            }
        }
    }
}
