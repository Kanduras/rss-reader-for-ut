import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import QtQuick.XmlListModel 2.0

Page {
    visible: false

    property var rssName
    property var feedUrl

    header: PageHeader {
        title: rssName
    }

    style: Rectangle {
        anchors.fill: parent
        color: theme.palette.normal.background
    }

    XmlListModel {
        id: xmlModel
        source: feedUrl
        query: "/rss/channel/item"

        XmlRole { name: "headlines"; query: "title/string()" }
        XmlRole { name: "pubDate"; query: "pubDate/string()" }
        XmlRole { name: "description"; query: "description/string()" }
        XmlRole { name: "link"; query: "link/string()"}
    }

    LomiriListView {
        id: lomiriListView
        anchors.fill: parent

        model: xmlModel
        delegate: ListItem {
            objectName: model.objectName
            //enabled: source != ""
            ListItemLayout {
                id: layout
                title.text: headlines
                ProgressionSlot {}
            }

            onClicked: {
                pStack.push(Qt.resolvedUrl("RssItem.qml"), {"headline": headlines, "description": description, "link": link})
            }
        }
    }
}
