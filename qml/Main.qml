/*
 * Copyright (C) 2024  Julius Zirnstein
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * rssreader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "storage"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'rssreader.kanduras'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    FeedModels {
        id: database
    }

    Settings {
        id: globalSettings
        property real version: 1.0
    }

    Settings {
        id: inset
        property real name: 1.444
    }

    PageStack {
        id: pStack
        Component.onCompleted: push(mainPage)

        Page {
            id: mainPage
            anchors.fill: parent

            visible: false


            header: PageHeader {
                id: header
                title: i18n.tr("RSS Feeds")

                trailingActionBar.actions: [
                    Action {
                        text: i18n.tr("Settings")
                        iconName: 'settings'
                        onTriggered: pStack.push(Qt.resolvedUrl("Settings.qml"), {"db": database})
                    }
                ]
            }

            LomiriListView {
                id: lomiriListView
                anchors { left: parent.left; right: parent.right;
                    top: header.bottom;
                    bottom: parent.bottom

                }

                model: database.feedsModel
                delegate: ListItem {
                    objectName: model.objectName
                    //enabled: source != ""
                    ListItemLayout {
                        id: layout
                        title.text: name
                        ProgressionSlot {}
                    }

                    leadingActions: ListItemActions {
                        actions: Action {
                            iconName: "delete"
                            onTriggered: {
                                database.removeFeed(feedId)
                            }
                        }
                    } 
                    onClicked: pStack.push(Qt.resolvedUrl("Feed.qml"), {"rssName": name, "feedUrl": url})
                }
            }
        }
    }
}
